import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_hosts_file(host):
    f = host.file('/etc/hosts')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'

def test_java_installed(host):
    java = host.package('openjdk-8-jdk')
    assert java.is_installed

    java = host.run('java -version').stderr.split('\n')[0]
    javac = host.run('javac -version').stderr.split()[1]

    # compare java and javac versions
    assert java == 'openjdk version "%s"' % javac