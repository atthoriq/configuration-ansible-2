import os

import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

@pytest.fixture()
def InitializeVars(host):
    ansible_vars = host.ansible(
        "include_vars", "file=../../vars/main.yml")
    return ansible_vars["ansible_facts"]

def test_hosts_file(host):
    f = host.file('/etc/hosts')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'

def test_timezone(host, InitializeVars):
    timezone = InitializeVars['timezone_zone']
    curr_timezone = host.run("cat /etc/timezone").stdout.split('\n')[0]
    assert curr_timezone == timezone

def test_ntp_installed_and_run(host):
    assert host.package('ntp').is_installed
    assert host.service('ntp').is_running

def test_ntp_server_id(host, InitializeVars):
    servers = InitializeVars['ntp_servers']
    ntp_conf = host.run("cat /etc/ntp.conf").stdout

    for server in servers:
        assert server in ntp_conf
