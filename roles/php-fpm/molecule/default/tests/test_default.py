import os

import re
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

@pytest.fixture()
def AnsibleVars(host):
    ansible_vars = host.ansible(
        "include_vars", "file=../../vars/main.yml")
    return ansible_vars["ansible_facts"]

def test_hosts_file(host):
    f = host.file('/etc/hosts')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'

def test_nginx_installed(host):
    assert host.package('nginx').is_installed

def test_php_packages_installed(host, AnsibleVars):
    php_version = AnsibleVars['php_default_version_debian']
    php_packages = AnsibleVars['php_packages']
    
    for pkg in php_packages:
        pkg = re.sub(r"{{[A-Za-z0-9_ ]+}}", php_version, pkg)
        assert host.package(pkg).is_installed