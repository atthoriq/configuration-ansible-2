# Configuration Ansible 2

Create Ansible Playbook(s)
- Initialize Playbook

It contains 3 main tasks (Set timezone to Asia/Jakarta, Update all packages, set NTP to Indonesia)

- Java

[x] Install Java

[x] Set variable JAVA_HOME

- Python

[x] Install Python with specific version (version defined as variables). Limitation: target machine need to have installed python already.

- Node

[x] Install Node, NPM using Nodesource

[ ] Create npm global directory & set to PATH

[ ] Install packages

- PHP-FPM

[x] Install packages

[ ] Ser configurations

- nginx

[x] Install nginx

[ ] Set configurations

- Docker

[x] Install Docker

[x] Install Docker Compose

- Hardening

[x] Pull hardening with CIS Benchmark created by florianutz

Current problem: I have apache on my server and prot 80 is already allocated to apache. so the nginx failed to start.
